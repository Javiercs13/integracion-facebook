import sys
sys.path.append('.')
from flask import Blueprint, request
from controllers.facebookController import Facebook

facebook = Facebook()
facebookBP = Blueprint('facebookBP', __name__)

@facebookBP.route('/webhook')
def validateWebHook():
    return facebook.validate()

@facebookBP.route('/webhook', methods = ['POST'])
def receivedMessage():
    return facebook.receivedMessage()

@facebookBP.route('/send-message')
def sendMessage():
    return facebook.sendMessage()

@facebookBP.route('/send-postback-button')
def sendPostbackButton():
    return facebook.sendPostbackButton()

@facebookBP.route('/send-quick-replies')
def quickReplies():
    return facebook.quickReplies()

@facebookBP.route('/send-list')
def sendList():
    return facebook.sendList()

@facebookBP.route('/send-media-template')
def sendMedia():
    return facebook.sendMedia()