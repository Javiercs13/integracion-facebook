import os
from dotenv import load_dotenv #importamos la liberia dotenv
load_dotenv()

# Creamos la clase Enviroment
class Enviroment():
    def envfacebook(self):
        return {
            'VERIFY_TOKEN': os.getenv('VERIFY_TOKEN', 'NONE'),
            'ACCESS_TOKEN': os.getenv('ACCESS_TOKEN', 'NONE'),
            'URL_API_GRAPH': os.getenv('URL_API_GRAPH', 'NONE')
            }

