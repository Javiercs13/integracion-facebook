import requests
import json
from utils.enviroment import Enviroment as Env

class SendRequests(Env):

    # Tenemos las variables de entorno vacias
    # a fin de asignarles valores luego
    # mediante los metodos config
    def __init__(self):
        self.URL_API_GRAPH = None
        self.params = None
        self.headers = None

    # establecemos como parametros el servicio a usar
    # si es Facebook u otro, luego el endpoint que
    # seria me/messages y la data
    def get(self, service, endpoint, data = None):
        self.optionConfig(service)

        response = requests.get(
            f'{self.URL_API_GRAPH}/{endpoint}',
            params=self.params,
            headers=self.headers,
            data = json.dumps(data)
            )
        if response.status_code != 200:
            self.log(response.status_code)
            self.log(response.text)

        else:
            return response.text

    # establecemos como parametros el servicio a usar
    # si es Facebook u otro, luego el endpoint que
    # seria me/messages y la data
    def post(self, service, endpoint, data = None):

        self.optionConfig(service)

        response = requests.post(
            f'{self.URL_API_GRAPH}/{endpoint}',
            params = self.params,
            headers = self.headers,
            data = json.dumps(data)
        )

        if response.status_code != 200:
            self.log(response.status_code)
            self.log(response.text)

        else:
            return response.text

    # Cuando tengamos mas servicios, nos interesa poder
    # acceder a las rutas de cada uno llamandolo
    # como parametro. En este caso Facebook
    def optionConfig(self, service):
        if service.upper() == 'FACEBOOK':
            self.configFacebook()

    def configFacebook(self):
        # Almacenamos las variables env
        envFacebook = self.envfacebook()
        # print(envFacebook) Lo comentamos para no mostrarlo en consola
        self.VERIFY_TOKEN = envFacebook['VERIFY_TOKEN']
        self.ACCESS_TOKEN = envFacebook['ACCESS_TOKEN']
        self.URL_API_GRAPH = envFacebook['URL_API_GRAPH']

        # Configuramos los parametros generales para el envio
        # de informacion a Facebook
        self.params = {"access_token": self.ACCESS_TOKEN}
        self.headers = {"Content-Type": "application/json"}

    def log(self, message):
        print('Log send request')
        print(json.dumps(message, indent=2))
