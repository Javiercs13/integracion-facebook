# -*- coding: utf-8 -*-
import sys
sys.path.append(".")
from flask import Flask

def create_app():

    # Creamos la aplicacion con flask
    app = Flask(__name__)

    from routes.facebookRoutes import facebookBP
    app.register_blueprint(facebookBP, url_prefix='/facebook')

    return app

# Ponemos el puerto al cual se debe conectar
if __name__ == '__main__':
    app = create_app()
    app.run(port=8000, debug=True)