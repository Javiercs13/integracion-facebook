
from flask import request
import json
from utils.sendRequests import SendRequests

#Hacemos que Controller herede los metodos de Enviroment
class Facebook(SendRequests):
    def validate(self):
        envFacebook = self.envfacebook()

        valueHubMode = request.args.get('hub.mode')
        valueHubChallenge = request.args.get('hub.challenge')
        valueHubToken = request.args.get('hub.verify_token')
        # Colocamos la logica de Facebook y agregamos el token importado
        # Solo accede con el token
        if valueHubMode == 'subscribe' and valueHubChallenge:
            print('Verificado')
            return request.args.get('hub.challenge','')
        # Si no es el token, no funcionara
        if not valueHubToken == envFacebook['VERIFY_TOKEN']:
            return "Lo siento. No es el token", 403

        return valueHubChallenge, 200

    def sendMessage(self):
        #Obtenemos la data enviada en el body en formato JSON
        data = request.get_json()
        recipientID = data['recipientID']
        messageText = data['messageText']
        data = {
            "recipient": {"id": recipientID},
            "message": {"text": messageText}
        }
        # Activamos el efecto de escribiendo
        self.sendAction(recipientID, 'typing_on')
        # Enviamos el mensaje a facebook
        return self.post('facebook', 'me/messages', data)

    def receivedMessage(self):
        data = request.get_json()
        # Imprime lo que recibimos temporalmente
        self.logController(data)
        # Valida el tipo del mensaje
        self.validateOption(data)

        return 'Mensaje recibido', 200

    def sendAction(self, recipientID, senderAction):
        data = request.get_json()
        # sendAction solo acepta mark seen, typing on y off
        data = {
            "recipient": {
                    "id": recipientID
                },
                "sender_action": senderAction
            }

        self.post('facebook', 'me/messages', data)

    def validateOption(self, data):
        if data['object'] == 'page':
            for entry in data['entry']:
                for messagingEvent in entry['messaging']:
                    senderID = messagingEvent['sender']['id']
                    self.logController(messagingEvent)

                    if messagingEvent.get('message'):
                        # Marca el mensaje como leido
                        self.sendAction(senderID, 'mark_seen')

                        # Alguien envia un mensaje y capturamos el texto
                        if messagingEvent['message'].get('text'):
                            message_text = messagingEvent['message']['text']
                            self.logController(f'El usuario nos envia el texto {message_text}')
                        # Alguien envia una imagen y capturamos el url
                        if messagingEvent['message'].get('attachments'):
                            message_image = dict(messagingEvent['message']['attachments'][0])
                            url = message_image['payload']['url']
                            self.logController(f'El usuario nos envia la imagen {url}')


                    # Confirmacion de delivery
                    if messagingEvent.get('delivery'):
                        # Desactiva el efecto de estar escribiendo
                        self.sendAction(senderID, 'typing_off')
                        self.logController('Confirmacion del delivery')

                    # Confirmacion del option
                    if messagingEvent.get('optin'):
                        self.logController('Confirmacion de Optin')

                    # Confirmacion de click en botones
                    if messagingEvent.get('postback'):
                        self.sendAction(senderID, 'mark_seen')
                        self.logController('Evento cuando usuario hace clic')

    def sendPostbackButton(self):
        data = request.get_json()

        recipientID = data['recipientID']
        data = {
            "recipient": {"id": recipientID},
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "button",
                        "text": "Que equipo eres",
                        "buttons": [
                            {
                                "type": "postback",
                                "title": "Alianza Lima",
                                "payload": "alianzaLima"
                            },
                            {
                                "type": "postback",
                                "title": "Universitario",
                                "payload": "universitario"
                            }
                        ]
                    }
                }
            }
        }
        self.sendAction(recipientID, 'typing_on')
        return self.post('facebook', 'me/messages', data)

    def quickReplies(self):
        data = request.get_json()

        recipientID = data['recipientID']
        data = {
        "recipient":{
            "id":recipientID
            },
            "messaging_type": "RESPONSE",
            "message": {
                "text": "De que color es tu casa",
                "quick_replies":[
                    {
                        "content_type":"text",
                        "title":"Rojo",
                        "payload":"rojo",
                        "image_url":"http://example.com/img/red.png"
                    },
                    {
                        "content_type":"text",
                        "title":"Verde",
                        "payload":"verde",
                        "image_url":"http://example.com/img/green.png"
                    }
                ]
            }
        }
        self.sendAction(recipientID, 'typing_on')
        return self.post('facebook', 'me/messages', data)

    def sendList(self):
        data = request.get_json()
        true = True
        false = False
        recipientID = data['recipientID']
        data = {
            "recipient": {"id": recipientID},
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "list",
                        "top_element_style": "compact",
                        "elements": [
                            {
                                "title": "Classic T-Shirt Collection",
                                "subtitle": "See all our colors",
                                "image_url": "https://scontent.flim1-1.fna.fbcdn.net/v/t1.0-9/p960x960/76635069_2600080833371062_1972817090532343808_o.jpg?_nc_cat=106&_nc_ohc=I1ZgZWppN_4AX8Xjkoy&_nc_ht=scontent.flim1-1.fna&_nc_tp=6&oh=f3cdf60537c5bb8ac67181a05cf4a98d&oe=5EB7FA76",
                                "buttons": [
                                    {
                                        "title": "View",
                                        "type": "web_url",
                                        "url": "https://peterssendreceiveapp.ngrok.io/collection",
                                        "messenger_extensions": true,
                                        "webview_height_ratio": "tall",
                                        "fallback_url": "https://peterssendreceiveapp.ngrok.io/"
                                    }
                                ]
                            },
                            {
                                "title": "Classic White T-Shirt",
                                "subtitle": "See all our colors",
                                "default_action": {
                                "type": "web_url",
                                "url": "https://peterssendreceiveapp.ngrok.io/view?item=100",
                                "messenger_extensions": false,
                                "webview_height_ratio": "tall"
                                }
                            },
                            {
                                "title": "Classic Blue T-Shirt",
                                "image_url": "https://scontent.flim1-1.fna.fbcdn.net/v/t1.0-9/p960x960/76635069_2600080833371062_1972817090532343808_o.jpg?_nc_cat=106&_nc_ohc=I1ZgZWppN_4AX8Xjkoy&_nc_ht=scontent.flim1-1.fna&_nc_tp=6&oh=f3cdf60537c5bb8ac67181a05cf4a98d&oe=5EB7FA76",
                                "subtitle": "100 Cotton, 200 Comfortable",
                                "default_action": {
                                    "type": "web_url",
                                    "url": "https://peterssendreceiveapp.ngrok.io/view?item=101",
                                    "messenger_extensions": true,
                                    "webview_height_ratio": "tall",
                                    "fallback_url": "https://peterssendreceiveapp.ngrok.io/"
                                    },
                                "buttons": [
                                    {
                                        "title": "Shop Now",
                                        "type": "web_url",
                                        "url": "https://peterssendreceiveapp.ngrok.io/shop?item=101",
                                        "messenger_extensions": true,
                                        "webview_height_ratio": "tall",
                                        "fallback_url": "https://peterssendreceiveapp.ngrok.io/"
                                    }
                                    ]
                                }
                            ],
                        "buttons": [
                            {
                                "title": "Ver mas",
                                "type": "postback",
                                "payload": "payload"
                            }
                            ]
                        }
                    }
                }
            }
        self.sendAction(recipientID, 'typing_on')
        return self.post('facebook', 'me/messages', data)

    def sendMedia(self):
        data = request.get_json()

        recipientID = data['recipientID']

        data = {
            "recipient":{"id": recipientID},
            "message":{
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "media",
                        "elements": [{
                            "media_type": "image",
                            "url": "https://www.facebook.com/AweikMedia/photos/p.2200364293342720/2200364293342720/?type=1&theater"
                            }
                                     ]
                        }
                    }
                }
            }
        self.sendAction(recipientID, 'typing_on')
        return self.post('facebook', 'me/messages', data)


    def logController(self, data):
        # Queremos que bote la data capturada en formato json e indentado
        print('Log Facebook')
        print(json.dumps(data, indent=2))